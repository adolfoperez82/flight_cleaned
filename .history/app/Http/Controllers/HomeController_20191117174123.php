<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $username = '';
    protected $password = '';
    protected $baseUrl = 'http://tstapi.duckdns.org/api/json/1F';

    public function __construct()
    {    
        $this->username = env('API_USERNAME'. 'php-applicant');
        $this->password = env('API_PASSWORD', 'Z7VpVEQMsXk2LCBc');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
       
        $url = $this->baseUrl.'/flightroutes';
        $request = $this->sendRequest($url);
        dd($request);
        $departures = $this->extractDepartures($request);

        return view('welcome',['departures' => $departures]);
    }

    public function getRoutesFromDeparture($departure){
        $url = $this->baseUrl."/flightroutes/?departureairport=$departure";
        $request = $this->sendRequest($url);
        $toDepartures = $this->extractDepartures($request, false);
        return json_encode($toDepartures);
    }

    public function getSchedule(Request $request){
        $parameters = $request->post();
        $from = $parameters['departureFrom'];
        $return = $parameters['departureReturn'];
        $url = $this->baseUrl."/flightschedules/?departureairport=$from&destinationairport=$return&returndepartureairport=$return&returndestinationairport=$from";

        $request = $this->sendRequest($url);
        return  json_encode($request);
    }

    public function searchFlights(Request $request){
        $parameters = $request->post();
        $oneWay = $parameters['round_trip_one_way'];        
        $from=$this->parseDate($parameters['from']);
        $return=$parameters['return'];
        $departure=$parameters['departure'];
        
        if(!$oneWay){
            $to=$this->parseDate($parameters['to']);
            $url = $this->baseUrl."/flightavailability?departuredate=$from&departureairport=$departure&destinationairport=$return&returndepartureairport=$return&returndestinationairport=$departure&returndate=$to";
        } else {
            $url = $this->baseUrl."/flightavailability?departuredate=$from&departureairport=$departure&destinationairport=$return";
        }
        
        $request = $this->sendRequest($url);
        $outs = [];
        foreach($request->flights->OUT as $flights){

            $arrival_time = strtotime($flights->datetime);           
            $arrival_hour =$arrival_time+$this->convertTimeToSeconds($flights->duration);

            $out = [
                'hour' => date('H:i:s', strtotime($flights->datetime)),
                'duration' => $flights->duration,
                'price' => $flights->price,
                'seats' => $flights->seatsAvailable,
                'airport' => $flights->depart->airport->code.' '.$flights->depart->airport->name,
                'arrival_airport' => $flights->arrival->airport->code.' '.$flights->arrival->airport->name,
                'arrival_time' =>  date('H:i:s',$arrival_hour)
            ];
            $outs[] = $out;
        }
        $rets = [];
        if(!$oneWay){
            foreach($request->flights->RET as $flights){

                $arrival_time = strtotime($flights->datetime);           
                $arrival_hour =$arrival_time+$this->convertTimeToSeconds($flights->duration);

                $ret = [
                    'hour' => date('H:i:s', strtotime($flights->datetime)),
                    'duration' => $flights->duration,
                    'price' => $flights->price,
                    'seats' => $flights->seatsAvailable,
                    'airport' => $flights->depart->airport->code.' '.$flights->depart->airport->name,
                    'arrival_airport' => $flights->arrival->airport->code.' '.$flights->arrival->airport->name,
                    'arrival_time' =>  date('H:i:s',$arrival_hour)
                ];
                $rets[] = $ret;
            }
        }

        $data = [
            'from' => $parameters['from'],
            'to' => $parameters['to'],
            'departure' => $departure,
            'return' => $return,
            'out_date' => $parameters['from'],
            'ret_date' => $parameters['to'],
            'out' => $outs,
            'ret' => $rets,
            'oneWay' => $oneWay,
            'adults' => $parameters['adults'],
            'children' => $parameters['children'],
            'babies' => $parameters['babies']
        ];

        return view('flightselector',['flights' => $data]);
    }

    public function bookPreview(Request $request){
        $parameters = $request->post();  
        return view('bookpreview',['book' => $parameters]);
    }

    private function extractDepartures($output, $from = true){
        $departures = [];
        $fromTo = ($from) ? 'Dep': 'Ret';
        $code = $fromTo.'Code';
        $name = $fromTo.'Name';

        foreach($output->flightroutes as $route){
            $departures[$route->$code] = $route->$name;
        }
        return $departures;
    }

    private function sendRequest($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "$this->username:$this->password");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $output = json_decode(curl_exec($ch));
        dd('jarl',$output);
        $info = curl_getinfo($ch);
        curl_close($ch);
        return $output;
    }

    private function parseDate($date){
        return str_replace('-','',$date);
    }

    private function convertTimeToSeconds($time){
        $parsed = date_parse($time);
        $seconds = $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
        return $seconds;
    }
}
