<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="{{ asset('js/form.js') }}"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

        <!-- Styles -->
        <style>
          
        </style>
    </head>
    <body>
            <div class=" block clear">
                <h3>Outbound Flight:</h3>
                <hr/>
                <p class="line"> {{$book['from']}}</p>
                <p class="line">
                    {{$book['departure']}} ({{$book['out_hour']}}) -> {{$book['out_arrival_airport']}} ({{$book['out_arrival_time']}})
                    <b>{{$book['out_price']}} €</b>
                </p>
                <div class="clear"></div>
            </div>
            @if(!$book['oneWay'])
            <div class=" block clear">
                <h3>Flight back:</h3>
                <hr/>
                <p class="line"> {{$book['to']}}</p>
                <p class="line">
                    {{$book['return']}} ({{$book['ret_hour']}}) -> {{$book['ret_arrival_airport']}} ({{$book['ret_arrival_time']}})
                    <b>{{$book['ret_price']}} €</b>
                </p>
                <div class="clear"></div>
            </div>
            @endif
            <div class=" block clear">
                <h3>Passengers:</h3>
                <hr/>
                <p class="line">Adults: {{$book['adults']}}</p>
                @if($book['children']>0)
                <p class="line">Children: {{$book['children']}}</p>
                @endif
                @if($book['babies']>0)
                <p class="line">Babies: {{$book['babies']}}</p>
                @endif
                <div class="clear"></div>
            </div>
            <div class=" block clear">
                <h3>Total:</h3>
                <hr/>
                <p class="line">{{$book['ret_price']+$book['out_price']}} €</p>
                <div class="clear"></div>
            </div>
           
    </body>
</html>
