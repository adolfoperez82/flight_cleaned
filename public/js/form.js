$(document).ready(function() {   
   initialice();   
});

function initialice(){
    $("select[name=departure]").change(selectToDeparture);
    $("select[name=return]").change(getSchedule);
    $("input[name=round_trip_one_way]").change(selectRoundTripOneWay);
    $(".book_form").submit(bookFormSubmit);

    $(".datepicker").datepicker({ dateFormat: 'yy-mm-dd' });
}

function bookFormSubmit(event){
    var oneWay = $('input[name=oneWay]').val();  
    var data = [];
    data['oneWay'] = oneWay;
    var out = $('input[name=out_time]:checked');
    $('input[name=out_hour]').val(out.data('hour'));
    $('input[name=out_price]').val(out.data('price'));
    $('input[name=out_arrival_airport]').val(out.data('arrival_airport'));
    $('input[name=out_arrival_time]').val(out.data('arrival_time'));

    if(oneWay == 0){
        var ret = $('input[name=ret_time]:checked');
        $('input[name=ret_hour]').val(ret.data('hour'));
        $('input[name=ret_price]').val(ret.data('price'));
        $('input[name=ret_arrival_airport]').val(ret.data('arrival_airport'));
        $('input[name=ret_arrival_time]').val(ret.data('arrival_time'));
    }
}

function selectToDeparture(){
    var departure =this.value;
    $.ajax({
        type: "GET",       
        url: './getRoutesFromDeparture/'+departure,
        success: function(r){
            $("select[name=return]").html("<option value=''>To...</option>");
                $.each( r, function( key, value ) {                   
                    $("select[name=return]").append($("<option></option>")
                                .attr("value",key)
                                .text(value)); 
                });
        },
        dataType: 'json'
      });    
}

function selectRoundTripOneWay(){
    var trip =this.value;
    if(trip == 1){
        $('.round-trip').hide();
    } else {
        $('.round-trip').show();
    }
}

function getSchedule(){
    var departureFrom = $("select[name=departure]").val();
    var departureReturn = $("select[name=return]").val();
    var csrf = $("input[name=_token]").val();
    $.ajax({
        type: "POST",       
        url: './getSchedule',
        data: {departureFrom:departureFrom, departureReturn:departureReturn, _token:csrf},
        success: function(r){
            var from = r.flightschedules.OUT;
            var to = r.flightschedules.RET;
            console.log(from);
            $("select[name=from]").html("<option value=''>From...</option>");
                $.each( from, function( i, value) {                   
                    $("select[name=from]").append($("<option></option>")
                                .attr("value",value.date)
                                .text(value.date)); 
                });
            $("select[name=to]").html("<option value=''>To...</option>");
                $.each( to, function( key, value ) {                   
                    $("select[name=to]").append($("<option></option>")
                    .attr("value",value.date)
                    .text(value.date)); 
                });
           

        },
        dataType: 'json'
      });
}

function make_base_auth(user, password) {
    var tok = user + ':' + password;
    var hash = Base64.encode(tok);
    return "Basic " + hash;
  }